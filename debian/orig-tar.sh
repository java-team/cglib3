#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>

VERSION=$(dpkg-parsechangelog | sed -ne 's,^Version: \(.*\)-.*,\1,p')
SOURCE=$(dpkg-parsechangelog | sed -ne 's,Source: \(.*\),\1,p')
TAR=../${SOURCE}_${VERSION}.orig.tar.gz
DIR=cglib3-$VERSION

# clean up the upstream tarball
mkdir $DIR
(cd $DIR && jar xf ../$3)
tar -c -z -f $TAR --exclude '*/lib/*' $DIR
rm -rf $3 $DIR

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi

